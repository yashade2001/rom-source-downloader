#!/bin/bash

bold=`tput bold`
normal=`tput sgr0`
yellow='\e[1;33m'
nocolor='\e[0m'

echo -e " $bold $yellow WELCOME TO AUTOMATIC ROM SOURCE DOWNLOADER SCRIPT $nocolor $normal "

echo -e "Firstly, I'm going to install needed packages for building Android..."

curl http://raw.githubusercontent.com/yashade2001/build-environment-preparative/master/prepare.sh > ~/prepare.sh
chmod a+x ~/prepare.sh
cd ~/

echo -e "This may take a few minutes. Enter your password if it asks."

./prepare.sh


echo -e "Select a ROM to download its source:";
echo -e "";

echo "
        1) Cyanogenmod
        2) AOSPA
        3) AOKP
        4) SaberDroid
	5) AOSPAL
	6) AOSPA-Legacy
	7) AICP
	8) C-RoM
	9) Carbon
	10) Slim
	11) Omni
	12) Dirty Unicorns
	13) VanirAOSP
	14) MoKee
	15) PAC
	16) MIUI
"

			read n

         case $n in

        1) echo -e "Write branch name and hit enter:"
		read b1

		echo -e "Downloading source to ~/cyanogenmod/"
		mkdir -p ~/cyanogenmod
		cd ~/cyanogenmod
		
repo init -u git://github.com/CyanogenMod/android.git -b $b1
repo sync		

               ;;
        2) echo -e "Write branch name and hit enter:"
		read b2

		echo -e "Downloading source to ~/aospa/"
		mkdir -p ~/aospa
		cd ~/aospa
		
repo init -u git://github.com/AOSPA/manifest.git -b $b2
repo sync
               ;;
        3) echo -e "Write branch name and hit enter:"
		read b3

		echo -e "Downloading source to ~/aokp/"
		mkdir -p ~/aokp
		cd ~/aokp
		
repo init -u git://github.com/AOKP/platform_manifest.git -b $b3
repo sync
               ;;
#        4) 
#               ;;
#        5) 
#               ;;
#        6) 
#               ;;
#        7) 
#               ;;
#        8) 
#               ;;
#        9) 
#               ;;
#        10) 
#               ;;
#        11) 
#               ;;
#        12) 
#               ;;
#        13) 
#               ;;
#        14) 
#               ;;
#        15) 
#               ;;
#        16) 
#              ;;

        *) invalid option
               ;;
         esac
